/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, PHP, Ruby, 
C#, VB, Perl, Swift, Prolog, Javascript, Pascal, HTML, CSS, JS
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>


int main(void)
{
 int *pPuntero;
 int var = 10;
 pPuntero = &var;

 printf("before ------ valor de var es: [%d]\n", var);
 printf("before --------valor de pPuntero es: [%d]\n", *pPuntero);

 var = 20;

 printf("after ------ valor de var es: [%d]\n", var);
 printf("after --------valor de pPuntero es: [%d]\n", *pPuntero);


 return 0 ;
}
