/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, PHP, Ruby, 
C#, VB, Perl, Swift, Prolog, Javascript, Pascal, HTML, CSS, JS
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
 #include <stdio.h>
#include <stdint.h>
#define TAM 10
int main(void)
{
 int array[10];

 for(int32_t i = 0; i < TAM; i++){
 array[i] = i;
 }
 for(int32_t i = 0; i < TAM; i++){
 printf(" [%d] ", array[i]);

 }
 printf("\n");
 array[5] = 500;
 int *pPointer = array;
 for(int32_t i = 0; i < TAM; i++){
 printf(" [%d] ", *(pPointer+i));

 }
 printf("\n");
 array[8] = 800;
 pPointer = &array[0];

 for(int32_t i = 0; i < TAM; i++){
 printf(" [%d] ", *(pPointer+i));

 }
 printf("\n");
 return 0;
}